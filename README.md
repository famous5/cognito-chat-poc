# Setup
* First Follow `ionic && Cordova` section in https://bitbucket.org/famous5/setup-guide/overview.
* Do `npm install`
* If you want to run on browser do `ionic serve`
## Android Setup
* If you want to run on emulator from android studio then,
	* First follow `Android setup guide` in https://bitbucket.org/famous5/setup-guide/overview
	* Once done do `ionic cordova build android` (make sure all is well)
	* Open Android Studio goto tools -> andrpod -> AVD Manager --> Click play next to emulator device u have created.
	* Make sure the Virtual device is one completly and then run `ionic cordova emulate android`
* If you wannd run Android device. Then,
	* Do `ionic cordova build android`
	* Make sure device is in developer mode and usb debugging is on  and plugin the device.
	* Run 'corodova run android'. 

