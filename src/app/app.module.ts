import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';

import { MyApp } from './app.component';

import { AngularFireAuth } from 'angularfire2/auth';
import { AngularFireModule } from 'angularfire2';

import { File } from '@ionic-native/file';
import { FileChooser } from '@ionic-native/file-chooser';
import { FilePath } from '@ionic-native/file-path';

import Amplify from 'aws-amplify';
import { fireBaseConfig } from './fireDbConfig';
import { AuthProvider } from '../providers/auth/auth';
import { DynamoDB } from '../providers/dynamodb/dynamodb';
import { ImghandlerProvider } from '../providers/imghandler/imghandler';
import { UserprofileProvider } from '../providers/userprofile/userprofile';
const aws_exports = require('../aws-exports').default;

Amplify.configure(aws_exports);

@NgModule({
  declarations: [
    MyApp
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp, { tabsPlacement: 'bottom' }),
    AngularFireModule.initializeApp(fireBaseConfig)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp
  ],
  providers: [
    StatusBar,
    SplashScreen,
    AngularFireAuth,
    { provide: ErrorHandler, useClass: IonicErrorHandler },
    AuthProvider,
    File,
    FileChooser,
    FilePath,
    DynamoDB,
    ImghandlerProvider,
    UserprofileProvider
  ]
})
export class AppModule { }
