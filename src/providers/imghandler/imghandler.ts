import { Injectable } from '@angular/core';
import { Storage } from 'aws-amplify';
import { FileChooser } from '@ionic-native/file-chooser';

/*
  Generated class for the ImghandlerProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class ImghandlerProvider {

  constructor(public filechooser: FileChooser) {
  }

  chooseimage() {
    var promise = new Promise<string>((resolve, reject) => {
      this.filechooser.open().then((url) => {
        (<any>window).FilePath.resolveNativePath(url, (result) => {
          resolve(result);
        });
      }).catch((err) => {
        reject(err);
      });
    });
    return promise;
  }

  saveProfilePic(photoUrl, userId) {
    var promise = new Promise<string>((resolve, reject) => {
      (<any>window).resolveLocalFileSystemURL(photoUrl, (res) => {
        res.file((resFile) => {
          const reader = new FileReader();
          reader.readAsArrayBuffer(resFile);
          const { type } = resFile;
          reader.onloadend = (evt: any) => {
            const imgBlob = new Blob([evt.target.result], { type: type });
            const uuid = this.guid();
            Storage.put(userId + '/' + uuid, imgBlob, { contentType: type })
              .then((res) => {
                resolve(uuid);
              }).catch((err) => {
                reject(err);
              });
          }
        });
      });
    });
    return promise;
  }


  getUserProfilePic(url) {
    return Storage.get(url, {})
      .then((url) => {
        return url.toString();
      }).catch((err) => {
        throw err;
      });
  }

  guid() {
    function s4() {
      return Math.floor((1 + Math.random()) * 0x10000)
        .toString(16)
        .substring(1);
    }
    return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
      s4() + '-' + s4() + s4() + s4();
  }
}
