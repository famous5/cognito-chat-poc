import { Injectable } from '@angular/core';
import { Auth, Logger } from 'aws-amplify';
//import { NavController } from 'ionic-angular';
const logger = new Logger('Login');
/*
  Generated class for the AuthProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class AuthProvider {
  login(loginDetails) {
    logger.info('login..');
    return Auth.signIn(loginDetails.username, loginDetails.password)
      .then(user => {
        return user;
      })
      .catch(err => logger.debug('error', err));
  }

  signup(details) {
    logger.debug('register');
    return Auth.signUp(details.username, details.password, details.email, details.phone)
      .then(user => {
        return user;
      })
      .catch(err => { return err; });
  }

  signInConfirm(user, code) {
    return Auth.confirmSignIn(user, code)
      .then((res) => {
        return res;
      })
      .catch(err => logger.debug('confirm error', err));
  }

  signUpConfirm(username, code) {
    return Auth.confirmSignUp(username, code)
      .then((res) => { return res; })
      .catch(err => logger.debug('confirm error', err));
  }

  resendCode(username) {
    return Auth.resendSignUp(username)
      .then(() => logger.debug('sent'))
      .catch(err => logger.debug('send code error', err));
  }

  getUserId() {
    return this.getcurrentUserCredentials();
  }

  getcurrentUserCredentials() {
    return Auth.currentAuthenticatedUser().then((res: any) => {
      return res.username;
    });
  }

  logout() {
    return Auth.signOut();
  }

  getcurrentAuthenticatedUser() {
    return Auth.currentAuthenticatedUser();
  }

  signOut() {
    return Auth.signOut()
      .then(data => console.log(data))
      .catch(err => console.log(err));
  }
}
