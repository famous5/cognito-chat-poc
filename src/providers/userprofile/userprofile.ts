import { Injectable } from '@angular/core';
import { DynamoDB } from '../dynamodb/dynamodb';
import { DocumentClient } from 'aws-sdk/clients/dynamodb';

const aws_exports = require('../../aws-exports.js').default;
/*
  Generated class for the UserprofileProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class UserprofileProvider {
  private userProfileTable: string = aws_exports.aws_resource_name_prefix + '-userProfile';
  constructor(public db: DynamoDB) {
  }

  saveUserProfile(userProfile) {
    const params = {
      'TableName': this.userProfileTable,
      'Item': userProfile,
      'ConditionExpression': 'attribute_not_exists(userId)'
    };
    return this.db.getDocumentClient()
      .then((client: DocumentClient) => { return client.put(params).promise(); })
      .then((res) => {
        return res;
      }).catch((err) => {
        throw err;
      });
  }

  getUserProfile(userId) {
    const params = {
      'TableName': this.userProfileTable,
      'KeyConditionExpression': "#userId = :userId",
      'ExpressionAttributeNames': { '#userId': 'userId' },
      'ExpressionAttributeValues': { ':userId': userId },
      'ScanIndexForward': false
    };
    return this.db.getDocumentClient()
      .then((client: DocumentClient) => { return client.query(params).promise(); })
      .then(data => { return data.Items; });
  }
}
