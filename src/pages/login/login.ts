import { Component } from '@angular/core';
import { IonicPage, NavController } from 'ionic-angular';
import { AuthProvider } from '../../providers/auth/auth';

/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {
  credentials: any = {};
  code: string;
  constructor(public navCtrl: NavController,
    public authservice: AuthProvider) {
  }

  passwordreset() {

  }

  ionViewDidLoad() {

  }

  signup() {
    this.navCtrl.push('SignupPage');
  }

  signin() {
    this.authservice.login(this.credentials).then((user: any) => {
      if (user && user.username) {
        this.navCtrl.push('TabsPage');
      } else {
        this.navCtrl.setRoot('LoginPage');
      }

    })
  }
}
