import { Component, NgZone } from '@angular/core';
import { IonicPage, NavController, LoadingController, AlertController, NavParams } from 'ionic-angular';
import { Logger } from 'aws-amplify';
import { ImghandlerProvider } from '../../providers/imghandler/imghandler';
import { UserprofileProvider } from '../../providers/userprofile/userprofile';
import { AuthProvider } from '../../providers/auth/auth';

const logger = new Logger('Account');
/**
 * Generated class for the ProfilepicPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-profilepic',
  templateUrl: 'profilepic.html',
})
export class ProfilepicPage {
  displayName: string;
  userId: string;
  public attributes: any;
  public selectedPhoto: Blob;
  public avatarPhoto: string = 'https://firebasestorage.googleapis.com/v0/b/myapp-4eadd.appspot.com/o/chatterplace.png?alt=media&token=e51fa887-bfc6-48ff-87c6-e2c61976534e';
  public nativepath: string;
  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public loadingCtrl: LoadingController,
    public alertCtrl: AlertController,
    public imgHandler: ImghandlerProvider,
    public userProfileService: UserprofileProvider,
    public authService: AuthProvider,
    public zone: NgZone) {
    // this.authService.getUserId().then((username: string) => {
    //   this.userId = username;
    //   this.displayName = username;
    // });

    this.userId = this.navParams.get('username');
    this.displayName = this.userId;
    this.attributes = [];
    this.selectedPhoto = null;
  }

  proceed() {
    this.navCtrl.setRoot('TabsPage');
  }

  chooseimage() {
    this.imgHandler.chooseimage().then((tempUrl) => {
      this.nativepath = tempUrl;
      this.avatarPhoto = tempUrl;
    });
  }

  refreshAvatar(id: string) {
    const photoRelativeUrl = this.userId + '/' + id;
    return this.imgHandler.getUserProfilePic(photoRelativeUrl).then((url) => {
      this.avatarPhoto = url;
      return photoRelativeUrl;
    });
  }

  save() {
    if (this.nativepath) {
      let loader = this.loadingCtrl.create({
        content: 'Please wait'
      });
      loader.present();
      this.imgHandler.saveProfilePic(this.nativepath, this.userId).then((fileName: string) => {
        return this.refreshAvatar(fileName);
      }).then((photoUrl) => {
        return this.saveUserProfile(photoUrl);
      }).then(() => {
        loader.dismiss();
        this.proceed();
      }).catch(err => logger.error(err));
    } else {
      this.proceed();
    }
  }

  saveUserProfile(fileName) {
    const userProfile = {
      userId: this.userId,
      displayName: this.displayName,
      photoUrl: fileName
    };
    return this.userProfileService.saveUserProfile(userProfile);
  }

  editname() {
    this.alertCtrl.create({
      buttons: ['okay']
    });
    let alert = this.alertCtrl.create({
      title: 'Edit Nickname',
      inputs: [{
        name: 'nickname',
        placeholder: 'Nickname'
      }],
      buttons: [{
        text: 'Cancel',
        role: 'cancel',
        handler: data => {

        }
      },
      {
        text: 'Edit',
        handler: data => {
          if (data.nickname) {
            this.zone.run(() => {
              this.displayName = data.nickname;
            });
          }
        }
      }]
    });
    alert.present();
  }
}