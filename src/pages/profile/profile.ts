import { Component, NgZone } from '@angular/core';
import { IonicPage, AlertController, LoadingController, App } from 'ionic-angular';
import { ImghandlerProvider } from '../../providers/imghandler/imghandler';
import { UserprofileProvider } from '../../providers/userprofile/userprofile';
import { AuthProvider } from '../../providers/auth/auth';
import { LoginPage } from '../login/login';

/**
 * Generated class for the ProfilePage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-profile',
  templateUrl: 'profile.html',
})
export class ProfilePage {
  avatar: string = 'https://firebasestorage.googleapis.com/v0/b/myapp-4eadd.appspot.com/o/chatterplace.png?alt=media&token=e51fa887-bfc6-48ff-87c6-e2c61976534e';
  displayName: string;

  constructor(public app: App,
    public userProfileService: UserprofileProvider,
    public zone: NgZone, public alertCtrl: AlertController,
    public imghandler: ImghandlerProvider,
    public authService: AuthProvider,
    public loadingCtrl: LoadingController) {
  }

  ionViewWillEnter() {
    this.loaduserdetails();
  }

  loaduserdetails() {
    let loader = this.loadingCtrl.create({
      content: 'Please wait'
    });
    loader.present();

    this.authService.getUserId().then((userId: string) => {
      return this.userProfileService.getUserProfile(userId);
    }).then((profile: any) => {
      if (profile && profile[0] && profile[0].photoUrl) {
        this.displayName = profile[0].displayName;
        return this.imghandler.getUserProfilePic(profile[0].photoUrl);
      }
    }).then((url) => {
      this.avatar = url;
      loader.dismiss();
    });
  }

  editimage() {
  }

  editname() {
  }

  logout() {
    this.authService.signOut()
      .then(() => this.app.getRootNav().setRoot(LoginPage));
  }
}