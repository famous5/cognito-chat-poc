import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ConfirmsignupPage } from './confirmsignup';

@NgModule({
  declarations: [
    ConfirmsignupPage,
  ],
  imports: [
    IonicPageModule.forChild(ConfirmsignupPage),
  ],
})
export class ConfirmsignupPageModule {}
