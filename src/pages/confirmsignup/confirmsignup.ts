import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { AuthProvider } from '../../providers/auth/auth';

/**
 * Generated class for the ConfirmsignupPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-confirmsignup',
  templateUrl: 'confirmsignup.html',
})
export class ConfirmsignupPage {

  public code: string;
  public username: string;

  constructor(public navCtrl: NavController, public navParams: NavParams,
    public authService: AuthProvider) {
    this.username = navParams.get('username');
  }

  confirm() {
    this.authService.signUpConfirm(this.username, this.code).then((res) => {
      this.navCtrl.push('ProfilepicPage', { 'username': this.username });
    }).catch((err) => {
      console.log(err);
    });
  }

  resendCode() {
    this.authService.resendCode(this.username);
  }
}
