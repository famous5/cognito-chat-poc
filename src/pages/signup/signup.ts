import { Component } from '@angular/core';
import { IonicPage, NavController, LoadingController, ToastController } from 'ionic-angular';
import { AuthProvider } from '../../providers/auth/auth';

/**
 * Generated class for the SignupPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-signup',
  templateUrl: 'signup.html',
})
export class SignupPage {
  newuser = {
    email: '',
    password: '',
    username: '',
    phone: ''
  }
  constructor(public navCtrl: NavController,
    public loadingCtrl: LoadingController,
    public toastCtrl: ToastController, public authService: AuthProvider) {
  }

  ionViewDidLoad() {
  }

  signup() {
    var toaster = this.toastCtrl.create({
      duration: 3000,
      position: 'bottom'
    });
    if (this.newuser.email == '' || this.newuser.password == '' || this.newuser.username == '' || this.newuser.phone == '') {
      toaster.setMessage('All fields are required');
      toaster.present();
    }
    else if (this.newuser.password.length < 7) {
      toaster.setMessage('Password is not strong. Try giving more than six characters');
      toaster.present();
    }
    else {
      let loader = this.loadingCtrl.create({
        content: 'Please wait'
      });
      loader.present();
      this.newuser.phone = "+1" + this.newuser.phone;
      this.authService.signup(this.newuser).then((res: any) => {
        loader.dismiss();
        if (res)
          this.navCtrl.push('ConfirmsignupPage', { username: this.newuser.username });
        else
          alert('Error' + res);
      })
    }
  }
  goback() {
    this.navCtrl.setRoot('LoginPage');
  }
}
