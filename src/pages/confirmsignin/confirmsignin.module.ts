import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ConfirmsigninPage } from './confirmsignin';

@NgModule({
  declarations: [
    ConfirmsigninPage,
  ],
  imports: [
    IonicPageModule.forChild(ConfirmsigninPage),
  ],
})
export class ConfirmsigninPageModule {}
