import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Events } from 'ionic-angular';

import { AuthProvider } from '../../providers/auth/auth';
/**
 * Generated class for the ConfirmsigninPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

//var SMS: any;

@IonicPage()
@Component({
  selector: 'page-confirmsignin',
  templateUrl: 'confirmsignin.html',
})
export class ConfirmsigninPage {


  public code: string;
  public user: any;

  constructor(public navCtrl: NavController, public navParams: NavParams,
    public authService: AuthProvider, public events: Events) {
    this.user = navParams.get('user');
  }

  confirm() {
    this.authService.signInConfirm(this.user, this.code).then((res) => {
      this.navCtrl.setRoot('TabsPage');
    }).catch((err) => {
      console.log(err);
    });
  }

  login() {
    this.navCtrl.push('LoginPage');
  }

}
